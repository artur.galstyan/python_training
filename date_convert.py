
M = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

def day_in_month(m):
	dim = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
	return dim[m - 1]

def check_y(y):
	return 0 < y < 3000

def check_m(m):
	return 0 < m <=12

def check_d(d, m):
	return 0 < d <= day_in_month(m)

def check_date(ndate):
	y, m, d = ndate
	return check_y(y) and check_m(m) and check_d(d, m)

	
def zero_pad(n, N):
	return "0" * (N-len(str(n))) + str(n)

def convert_m_to_Mon(m):
	if check_m(m):
		return M[m-1]
	else:
		return "wrong Mon"

def convert_Mon_to_m(Mon):
	for i in range(0, 12):
		if M[i] == Mon:
			return 1+i
	else:
		return -1000
def parse_date_yyyymmdd(date):
	ndate = tuple(int(i) for i in date.split("/"))
	if check_date(ndate):
		return ndate
	else:
		return ()

def parse_date_Mon_dd_yyyy(date):
	y = int(date.split()[2])
	m = convert_Mon_to_m(date[0: 3]) 
	d = int(date.split()[1][:-1])
	ndate = (y, m, d)
	if check_date(ndate):
		return (y, m, d)
	else:
		return ()

def format_to_yyyymmdd(ndate):
	y = ndate[0]
	m = ndate[1]
	d = ndate[2]
	return str(y) + "/" + zero_pad(str(m), 2) + "/" + zero_pad(str(d), 2)

def format_to_Mon_dd_yyyy(ndate):
	y = ndate[0]
	m = convert_m_to_Mon(ndate[1])
	d = ndate[2]
	return m + " " + zero_pad(str(d), 2) + ", " + str(y)
def date_convert(date):
	# input date format "2021/08/11" year/month/day
	return format_to_Mon_dd_yyyy(parse_date_yyyymmdd(date))



#def convert_date(date):
#	# input date format "Aug 11, 2021" month day, year
#	year = date.split()[2]
#	month = convert_Mon_to_m(date[0: 3]) 
#	day = zero_pad(date.split()[1][:-1], 2)
#	return year + "/" + month + "/" + day
