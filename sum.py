#!/usr/bin/python3


def sum(nums):
        S = 0
        for i in nums:
                S = S + i
        return S

if __name__ == "__main__":
        from sys import argv
        if argv[1] == "--test":
                #       ---test
                assert sum([])== 0
                assert sum([0])== 0
                assert sum([0, 0]) == 0
                assert sum([1, -1]) == 0
                assert sum([1, 2, 3, 4, 5]) == 15
                #---------
        else:
                print(sum([float(x) for x in argv[1:]]))


