from date_convert import (
	day_in_month,
	check_y,
	check_m,
	check_d,
	check_date,
	zero_pad,
	convert_m_to_Mon,
	convert_Mon_to_m,
	parse_date_yyyymmdd,
	parse_date_Mon_dd_yyyy,
	format_to_yyyymmdd,
	format_to_Mon_dd_yyyy,
	date_convert
	)


def test_day_in_month():
	assert day_in_month(1) == 31
	assert day_in_month(2) == 28
	

def test_check_y():
	assert check_y(2021) == True
	assert check_y(3021) == False
	
def test_check_m():
	assert check_m(1) == True
	assert check_m(13) == False 

def test_check_d():
	assert check_d(1, 1) == True 
	assert check_d(32, 1) == False

def test_check_date():
	assert check_date((2021, 8, 12)) == True
	assert check_date((3021, 8, 12)) == False 

def test_zero_pad():
	assert zero_pad(1, 4) == "0001"
	assert zero_pad(1111, 4) == "1111"

def test_convert_m_to_Mon():
	assert convert_m_to_Mon(8) == "Aug"
	assert convert_m_to_Mon(13) == "wrong Mon"

def test_convert_Mon_to_m():
	assert convert_Mon_to_m("Aug") == 8

def test_parse_date_yyyymmdd():
	assert parse_date_yyyymmdd("2021/08/12") == (2021, 8, 12)
	assert parse_date_yyyymmdd("2021/11/12") == (2021, 11, 12)
	assert parse_date_yyyymmdd("2021/11/32") == () 
	assert parse_date_yyyymmdd("2021/15/10") == () 
	assert parse_date_yyyymmdd("3021/08/10") == ()

def test_parse_date_Mon_dd_yyyy():
	assert parse_date_Mon_dd_yyyy("Aug 11, 2021") == (2021, 8, 11)
	assert parse_date_Mon_dd_yyyy("AAA 11, 2021") == ()
	assert parse_date_Mon_dd_yyyy("Aug 41, 2021") == ()
	assert parse_date_Mon_dd_yyyy("Aug 11, 3021") == ()

def test_format_to_yyyymmdd():
	assert format_to_yyyymmdd((2021, 8, 12)) == "2021/08/12"

def test_format_to_Mon_dd_yyyy():
	assert format_to_Mon_dd_yyyy((2021, 8, 12)) == "Aug 12, 2021"



def test_date_convert():
	assert date_convert("2021/08/11") == "Aug 11, 2021"

#def test_convert_date():
#	assert convert_date("Aug 11, 2021") == "2021/08/11" 
#	assert convert_date("Aug 1, 2021") == "2021/08/01" 
	
