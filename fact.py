#!/usr/bin/python3
"calculate factorial of n"

def fact_r(n):
	if n == 0:
		return 1
	else:
		return n*fact_r(n-1)

def fact_i(n):
	p = 1
	while n >= 1:
		p = p*n
		n = n-1
	return p

def fact_test():
	assert fact_r(0) == 1
	assert fact_r(1) == 1
	assert fact_r(2) == 2
	assert fact_r(3) == 6
	assert fact_r(4) == 24
	assert fact_r(10) == 3628800
	assert fact_i(0) == 1
	assert fact_i(1) == 1
	assert fact_i(2) == 2
	assert fact_i(3) == 6
	assert fact_i(4) == 24
	assert fact_i(10) == 3628800

fact = fact_i

if __name__ == "__main__":
	from sys import argv

	if len(argv) == 1:
		print("usage: fact n" )
	elif len(argv) == 2:
		if argv[1] == "--test":
			fact_test()
		else:
			x=int(argv[1])
			print(fact(x))
	elif len(argv) == 3:
		if argv[1] == "-r":
			fact = fact_r
		elif argv[1] == "-i":
			fact = fact_i
		x=int(argv[2])
		print(fact(x))
	else:
		print("error fact(x)")