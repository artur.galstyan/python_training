#!/usr/bin/python3
import sys
from p2p_dist import p2p_d

def print_help():
	print("Usage: p2p_dist_prog.py tetta1 fi1 tetta2 fi2")

if len(sys.argv) == 5:
	tetta_1=float(sys.argv[1])
	fi_1=float(sys.argv[2])
	tetta_2=float(sys.argv[3])
	fi_2=float(sys.argv[4])
	print(p2p_d(tetta_1, fi_1, tetta_2, fi_2))
elif len(sys.argv) == 2 and sys.argv[1]=="--help":
	print_help()
else:
	print("------------------ERROR:")
	print_help()