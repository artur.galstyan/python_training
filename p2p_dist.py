'''example for:
	2 point distance calculation for education'''
import math

R=6375000

def d2r(x):
	"convertin degree to radian"
	return x*math.pi/180


def dec_x(tetta, fi):
	"calculating x from tetta and fi"
	return R*math.cos(d2r(tetta))*math.cos(d2r(fi))
def dec_y(tetta, fi):
	"calculating y from tetta and fi"
	return R*math.cos(d2r(tetta))*math.sin(d2r(fi))
def dec_z(tetta):
	"z from tetta and fi"
	return R*math.sin(d2r(tetta))

def dec_xyz (tetta, fi): 
	"calculate x y z of tetta fi"
	x = dec_x(tetta, fi)
	y = dec_y(tetta, fi)
	z = dec_z(tetta)
	return x,y,z

def p2p_d(tetta_1, fi_1, tetta_2, fi_2):
	x_1, y_1, z_1 = dec_xyz(tetta_1, fi_1)
	x_2, y_2, z_2 = dec_xyz(tetta_2, fi_2)

	return ((x_1-x_2)**2+(y_1-y_2)**2+(z_1-z_2)**2)**0.5

#print("Distance =",(p2p_d(0,0,180,0)))
