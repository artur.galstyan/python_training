

def filter_list(l, pred):
    fl = []
    for c in l:
        if pred(c):
            fl = fl + [c]
    return fl

def is_even(n):
    return n%2 == 0

def is_odd(n):
    return n%2 != 0



def filter_even(l):
    return filter_list(l, is_even)


def filter_odd(l):
    return filter_list(l, is_odd)


assert filter_list([1, 2], is_even) == [2]
assert filter_list([1], is_even) == []


assert filter_even([]) == []
assert filter_even([1]) == []
assert filter_even([1, 2]) == [2]
assert filter_even([2]) == [2]

assert filter_odd([]) == []
assert filter_odd([1]) == [1]
assert filter_odd([1, 2]) == [1]
assert filter_odd([2]) == []

a = [1, 2, 3, 4]
assert filter_even(a) == [2, 4]
assert filter_odd(a) == [1, 3]
assert a == [1, 2, 3, 4]
