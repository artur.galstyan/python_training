from split_string import split_string, find_next_delim


def test_split_string():
	assert split_string("", " ") == [""]
	assert split_string(" ", " ") == ["", ""]
	assert split_string("a", " ") == ['a']
	assert split_string("a", " ") == ['a']
	assert split_string("asd 123 qwe") == ["asd", "123", "qwe"]  
	assert split_string("asd 123 qwe") == ["asd", "123", "qwe"]  
	assert split_string("asd#123#qwe", "#") == ["asd", "123", "qwe"]  
	assert split_string("asd#123#qwe", "#123#") == ["asd",  "qwe"]  

def test_find_delim():
	assert find_next_delim("a", "d", 1) == 1
	assert find_next_delim("aaa", "d", 0) == 3
	assert find_next_delim("aaa#bbb", "#", 0) == 3
	assert find_next_delim("aaa#bbb", "#", 0) == 3
