
def find_next_delim(input_string, delim, start_pos):
	dl = len(delim)
	end_pos = len(input_string) - dl + 1
	for i in range(start_pos, end_pos):
		if input_string[i:i+dl] == delim:
			return i
	return len(input_string)


def split_string(x, delim=" "):
	cws = 0  # current word start
	cwe = -1  # current word end 
	spl = [] # spleted string buffer
	l = len(x) #input string length
	dl = len(delim) # delimetr length
	while cwe < l:
		cwe = find_next_delim(x, delim, cws)
		spl = spl+[x[cws:cwe]]
		cws = cwe + dl 
	return spl



